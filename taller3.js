// función anónima 
let fa = function(){
    console.log("Función anónima");
}

// función flecha
let ff = (producto) =>{
    console.log(producto);
}

// función callback
function callback(){
    console.log("Callback")
}

function fcb(fn){
    fn();
}

